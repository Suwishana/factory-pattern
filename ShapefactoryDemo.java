package bcas.ap.dp.fact;

public class ShapefactoryDemo {
	public static void main(String[]args) {
	
		ShapeFactory ShapeFactory=new ShapeFactory();
		Shape Circle=ShapeFactory.CallShape(ShapeType.CIRCLE);
		
		Circle.draw();
		
		Shape Square =ShapeFactory.CallShape(ShapeType.SQUARE);
		Square.draw();
		
		Shape reactangle=ShapeFactory.CallShape(ShapeType.REACTANGLE);
		reactangle.draw();
		
		Shape pentagon=ShapeFactory.CallShape(ShapeType.PENTAGON);
		pentagon.draw();
		
	}

}
